from setuptools import setup, find_packages
from subprocess import check_output


def get_version_string():
    output = check_output(["git", "describe", "--tags"])
    parts = output.split('-')
    tag, count, sha = parts[:3]
    return "{}.dev{}+{}".format(tag, count, sha)

setup(
    name="s3_wrap",
    version=get_version_string(),
    description="S3 wrapper",
    author="Jarrett Egertson",
    author_email="jegertso@uw.edu",
    classifiers=['Development Status :: 3 - Alpha',
                 'Programming Language :: Python :: 2.7'],
    zip_safe=False,
    packages=find_packages(),
    install_requires=['botocore', 'boto3'],
    entry_points={
        'console_scripts': [
            's3_wrap = s3_wrap.scripts.s3_wrap:s3wrap'
        ]
    },)
